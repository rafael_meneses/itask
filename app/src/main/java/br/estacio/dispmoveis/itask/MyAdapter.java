package br.estacio.dispmoveis.itask;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import br.estacio.dispmoveis.itask.entity.Task;

/**
 * Created by rafael on 03/12/15.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>
{

    private List<Task> mTask;

    // Provide a reference to the type of views that you are using
    // (custom viewholder)
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTxtViewTask;
        public TextView mTxtViewDate;
        public TextView mTxtViewId;
        public ViewHolder(View v) {
            super(v);
            mTxtViewTask = (TextView) v.findViewById(R.id.titleTask);
            mTxtViewDate = (TextView) v.findViewById(R.id.dateTask);
            mTxtViewDate = (TextView) v.findViewById(R.id.idTask);
        }
    }


    public MyAdapter(List<Task> mTask) {
        this.mTask = mTask;
    }
    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.text_view_list, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.mTxtViewTask.setText(mTask.get(position).getTitle());
        holder.mTxtViewDate.setText(mTask.get(position).getFormatedDate());
        //holder.mTxtViewId.setText((mTask.get(position).getId() != null) ? mTask.get(position).getId().toString() : "");
        holder.mTxtViewId.setText("");

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mTask.size();
    }
}
