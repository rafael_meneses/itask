package br.estacio.dispmoveis.itask;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.estacio.dispmoveis.itask.database.DatabaseHelper;
import br.estacio.dispmoveis.itask.entity.Task;

public class TaskFormActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    public void btnSaveClick(View v)
    {
        Task task = new Task();
        EditText ed = (EditText) findViewById(R.id.txtTitle);
        Switch sw = (Switch) findViewById(R.id.switchDone);
        String title = ed.getText().toString();
        Boolean done = sw.isChecked();
        if (title != null)
            task.setTitle(title);
            task.setDone(done);
        try {
            Dao<Task, Integer> simpleDao = getHelper().getTaskDao();
            if (task.getId() != null)
                simpleDao.update(task);
            else
                simpleDao.create(task);

            ed.setText("");
            Snackbar.make(v, "Salvo", Snackbar.LENGTH_LONG).setAction("Action", null).show();

            Context context = getApplicationContext();
            CharSequence text = "Salvo";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

		/*
		 * You'll need this in your class to release the helper when done.
		 */
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

    /**
     * You'll need this in your class to get the helper from the manager once per class.
     */
    private DatabaseHelper getHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
}
