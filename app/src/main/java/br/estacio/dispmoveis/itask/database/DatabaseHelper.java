package br.estacio.dispmoveis.itask.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import br.estacio.dispmoveis.itask.entity.Task;

/**
 * Created by rafael.menezes on 02/12/2015.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "itask.db";
    private static final int DATABASE_VERSION = 2;
    private final String LOG_NAME = getClass().getName();

    private Dao<Task, Integer> taskDao;

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource)
    {
        try
        {
            TableUtils.createTable(connectionSource, Task.class);
        }
        catch (SQLException e)
        {
            Log.e(LOG_NAME, "Could not create one or more tables", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion)
    {
        try
        {
            TableUtils.dropTable(connectionSource, Task.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        }
        catch (SQLException e)
        {
            Log.e(LOG_NAME, "Could not upgrade one or more tables", e);
        }
    }

    public Dao<Task, Integer> getTaskDao() throws SQLException
    {
        if (taskDao == null)
        {
            taskDao = getDao(Task.class);
        }
        return taskDao;
    }

}
