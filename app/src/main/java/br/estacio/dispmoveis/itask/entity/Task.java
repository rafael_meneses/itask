package br.estacio.dispmoveis.itask.entity;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.AbstractMap;
import java.util.Date;

/**
 * Created by rafael.menezes on 02/12/2015.
 */
@DatabaseTable(tableName = "Task")
public class Task implements Comparable<Task>, Serializable
{
    private static final long serialVersionUID = 3L;

    @DatabaseField(generatedId = true)
    private Integer id;
    @DatabaseField
    private String title;
    @DatabaseField
    private Date createDate;
    @DatabaseField
    private Boolean done;

    public Task()
    {
        this.createDate = new Date();
        this.done = false;
    }

    public Task(Integer id)
    {
        this.id = id;
        this.createDate = new Date();
        this.done = false;
    }

    public Task(Integer id, String title)
    {
        this.id = id;
        this.title = title;
        this.createDate = new Date();
        this.done = false;
    }

    public Task(Integer id, String title, Date createDate)
    {
        this.id = id;
        this.title = title;
        this.createDate = createDate;
        this.done = false;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public Date getCreateDate()
    {
        return createDate;
    }

    public String getFormatedDate()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("d MMM yyyy");
        return sdf.format(this.createDate);
    }

    @Override
    public String toString()
    {
        return "Task{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", createDate=" + createDate +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Task task = (Task) o;

        if (getId() != null ? !getId().equals(task.getId()) : task.getId() != null) return false;
        if (getTitle() != null ? !getTitle().equals(task.getTitle()) : task.getTitle() != null)
            return false;
        return !(getCreateDate() != null ? !getCreateDate().equals(task.getCreateDate()) : task.getCreateDate() != null);

    }

    @Override
    public int hashCode()
    {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getTitle() != null ? getTitle().hashCode() : 0);
        result = 31 * result + (getCreateDate() != null ? getCreateDate().hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Task another)
    {
        if (this.id < another.getId())
        {
            return -1;
        }
        if (this.id > another.getId())
        {
            return 1;
        }
        return 0;
    }
}
